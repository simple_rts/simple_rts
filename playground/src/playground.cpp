#include "Window.h"

using namespace rts;

int
main(int argc, char** argv)
{
	//type code here.
	Window window = window_new(500, 700, nullptr, nullptr);

	int width  = window_width(window);
	int height = window_height(window);

	window_free(window);

	return 0;
}
