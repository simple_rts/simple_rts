#pragma once

#include "Exports.h"

namespace rts
{
	// Window
	typedef struct IWindow *Window;

	GRAPHICS_EXPORT Window
	window_new(int width, int height, void* os_handle, void* display);

	GRAPHICS_EXPORT void
	window_free(Window window);

	inline static void
	destruct(Window window)
	{
		window_free(window);
	}

	GRAPHICS_EXPORT int
	window_width(Window window);

	GRAPHICS_EXPORT int
	window_height(Window window);

	GRAPHICS_EXPORT void
	window_size_set(Window window, int width, int height);

	GRAPHICS_EXPORT void*
	window_native_handle(Window window);

	GRAPHICS_EXPORT void*
	window_user_data(Window window);

	GRAPHICS_EXPORT void
	window_user_data_set(Window window, void* ptr);

} // namespace rts