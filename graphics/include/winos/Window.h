#pragma once

// Window interface for Winos
namespace rts
{
	struct IWindow
	{
		HWND hwnd;
		HDC hdc;
		int width;
		int height;
		void *user_data;
	};
} // namespace rts
