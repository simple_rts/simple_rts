
#ifndef GRAPHICS_EXPORT_H
#define GRAPHICS_EXPORT_H

#ifdef GRAPHICS_STATIC_DEFINE
#  define GRAPHICS_EXPORT
#  define GRAPHICS_NO_EXPORT
#else
#  ifndef GRAPHICS_EXPORT
#    ifdef graphics_EXPORTS
        /* We are building this library */
#      define GRAPHICS_EXPORT 
#    else
        /* We are using this library */
#      define GRAPHICS_EXPORT 
#    endif
#  endif

#  ifndef GRAPHICS_NO_EXPORT
#    define GRAPHICS_NO_EXPORT 
#  endif
#endif

#ifndef GRAPHICS_DEPRECATED
#  define GRAPHICS_DEPRECATED __declspec(deprecated)
#endif

#ifndef GRAPHICS_DEPRECATED_EXPORT
#  define GRAPHICS_DEPRECATED_EXPORT GRAPHICS_EXPORT GRAPHICS_DEPRECATED
#endif

#ifndef GRAPHICS_DEPRECATED_NO_EXPORT
#  define GRAPHICS_DEPRECATED_NO_EXPORT GRAPHICS_NO_EXPORT GRAPHICS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef GRAPHICS_NO_DEPRECATED
#    define GRAPHICS_NO_DEPRECATED
#  endif
#endif

#endif /* GRAPHICS_EXPORT_H */
