#if OS_WINDOWS

#include "Window.h"

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#undef DELETE

#include "winos/Window.h"

#include <corecrt_malloc.h>
#include <assert.h>

namespace rts
{
	Window
	window_new(int width, int height, void* os_handle, void* display)
	{
		Window self		= (Window)::malloc(sizeof(IWindow));
		self->hwnd		= (HWND)os_handle;
		self->hdc		= GetDC(self->hwnd);
		self->width		= width;
		self->height	= height;
		self->user_data = nullptr;
		return self;
	}

	void
	window_free(Window window)
	{
		ReleaseDC(window->hwnd, window->hdc);
		::free(window);
	}

	int
	window_width(Window window)
	{
		return window->width;
	}

	int
	window_height(Window window)
	{
		return window->height;
	}

	void
	window_size_set(Window window, int width, int height)
	{
		assert(width  > 0 && "Width must be grater than zero");
		assert(height > 0 && "Height must be grater than zero");

		window->width	= width;
		window->height  = height;
	}

	void*
	window_native_handle(Window window)
	{
		return window->hwnd;
	}

	void*
	window_user_data(Window window)
	{
		return window->user_data;
	}

	void
	window_user_data_set(Window window, void* ptr)
	{
		window->user_data = ptr;
	}
} // namespace rst
#endif